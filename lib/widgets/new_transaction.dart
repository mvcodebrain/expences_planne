import 'package:expences_planne/widgets/adaptive_flat_botton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class NewTransaction extends StatefulWidget {
  final Function addNewTx;

  NewTransaction(this.addNewTx);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final titleControler = TextEditingController();
  final amountControler = TextEditingController();
  DateTime _selectedDate;

  void _submitedData() {
    if (amountControler.text.isEmpty) {
      return;
    }
    final titleData = titleControler.text;
    final amountData = double.parse(amountControler.text);

    if (titleData.isEmpty || amountData <= 0 || _selectedDate == null) {
      return;
    }

    widget.addNewTx(
      titleData,
      amountData,
      _selectedDate,
    );

    Navigator.of(context).pop();
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2019),
      lastDate: DateTime.now(),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        // padding: EdgeInsets.only(
        //   top: 10,
        //   left: 10,
        //   right: 10,
        //   bottom: MediaQuery.of(context).viewInsets.bottom + 10,
        // ),
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TextField(
              decoration: InputDecoration(
                labelText: 'Title',
              ),
              keyboardType: TextInputType.name,
              textCapitalization: TextCapitalization.sentences,
              controller: titleControler,
              onSubmitted: (_) => _submitedData(),
              autofocus: true,
              // onChanged: (val) => titleInput = val,
            ),
            TextField(
              decoration: InputDecoration(
                labelText: 'Amount',
              ),
              keyboardType: TextInputType.number,
              controller: amountControler,
              onSubmitted: (_) => _submitedData(),
              // onChanged: (val) => amountInput = val,
            ),
            Container(
              height: 70,
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      _selectedDate == null
                          ? 'No Date Choosen'
                          : 'Picked Date: ${DateFormat.yMd().format(_selectedDate)}',
                    ),
                  ),
                  AdaptiveButton('Choose Date', _presentDatePicker),
                ],
              ),
            ),
            RaisedButton(
              child: Text(
                'Add Transaction',
              ),
              color: Theme.of(context).primaryColor,
              textColor: Theme.of(context).textTheme.button.color,
              onPressed: _submitedData,
            ),
          ],
        ),
      ),
    );
  }
}
