import 'package:expences_planne/widgets/transaction_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/transaction.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteTxn;

  TransactionList(this.transactions, this.deleteTxn);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (cxt, constraints) {
      return transactions.isEmpty
          ? Column(
              children: [
                SizedBox(
                  height: constraints.maxHeight * 0.02,
                ),
                Text(
                  'No transactions found yet!',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: constraints.maxHeight * 0.6,
                  child: Image.asset(
                    'assets/images/no-data.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            )
          : ListView(
              children: transactions
                  .map(
                    (tx) => TransactionItem(
                      key: ValueKey(
                        tx.id,
                      ),
                      transaction: tx,
                      deleteTxn: deleteTxn,
                    ),
                  )
                  .toList(),
            );
    });
  }
}
